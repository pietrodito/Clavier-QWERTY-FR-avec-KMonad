Voici un aide-mémoire en français pour taper du français avec un clavier QWERTY
grâce à l'utilitaire [KMonad](https://github.com/kmonad/kmonad). Pourquoi faire
cela ?

+ Bénéficier des avantages du clavier QWERTY pour programmer, utiliser Vim
  etc...
+ Pouvoir tout de même taper les caractères accentués (même le Ÿ utile pour certains non de ville)
+ Sans utilser plusieurs dispositions de clavier
+ Apprendre à utilsier KMonad car avec vous pouvez rendre votre clavier plus
  puissant que vous ne l'avez jamais esperé
+ Au passage, nous allons voir comment facilement rendre la touche la plus
  contre-productive du clavier <CAPS-LOCK> en la plus importante surtout si vous
  utilisez Vim.

Attention, si vous tapez dans d'autres langues comme l'espagnol, le polonais...
je vous conseille d'utiliser la disposition QWERTY intl. with dead keys ou bien
d'utiliser plus avant KMonad.

_____
